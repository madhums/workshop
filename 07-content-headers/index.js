
var koa = require('koa');

var app = module.exports = koa();

app.use(function* () {
  var body = 'ok';
  if (this.request.is('json')) {
    this.response.type = 'json';
    body = {
      message: 'hi!'
    };
  }
  this.response.body = body;
})

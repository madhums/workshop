
var koa = require('koa');

var app = koa();

app.use(function *(next) {
  if (this.request.path !== '/') return yield next;
  this.response.body = 'hello world';
});

app.use(function *(next) {
  if (this.request.path !== '/404') return yield next;
  this.response.body = 'page not found';
  this.response.status = 404;
});

app.use(function *(next) {
  this.response.body = 'internal server error';
  this.response.status = 500;
})

module.exports = app;

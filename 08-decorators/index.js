
var koa = require('koa');
var escape = require('escape-html');
var html = 'this following HTML should be escaped: <p>hi!</p>';

var app = module.exports = koa();

app.use(function* (next) {
  yield next;
  this.response.body = escape(html);
});

app.use(function* body() {
  this.response.body = html;
});
